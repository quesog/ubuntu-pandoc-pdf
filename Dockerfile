FROM ubuntu:xenial

RUN apt update && apt install -y pandoc texlive texlive-* && apt clean && rm -rf /var/lib/apt/lists/* && rm -rf /usr/share/doc

CMD ["bash"]